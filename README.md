# Eclipse Project Code Review Creator

The Eclipse Project Code Review Creator creates IP review requests in IPLab for Eclipse Projects.

This tool identifies projects for which we are waiting for an initial contribution. An initial contribution takes the form of a new Git repository being identified in the project's metadata. When the tool encounters a Git repository that it hasn't seen before, it creates a review request record in IPLab with a pointer to the contents of the top commit in the default branch of the repository and then links that IPLab record to the issue being used by the EMO to track the project creation process.

This tool will only create a review for a repository when:

* It is not archived;
* It is not empty; and
* It is "interesting".

The current implementation checks _only the root_ of each repository and decides that a repository is interesting when it at least one file with a name that:

* does not start with a a dot;
* is not "otterdog"; and
* the name (not including the extension) entirely upper case.

The rules rely on file names that follow certain conventions. A file named "README.md" would not be considered interesting, but "Readme.md" (because it is not entirely upper case) would be considered interesting. Note that the test is more that the name part of the files does not contain any lower case characters (e.g., "EPL-2.0.txt" is not considered interesting despite having characters that are neither upper or lower case).

A file/directory named, for example, "doc" would be considered interesting.

This is a bit of a prototype/experiment. This implementation is neither complete, nor is it particularly robust. In its current form, it provides exactly what we need and nothing more.

## License

This project is distributed under the terms of the Eclipse Public License 2.0 (EPL-2.0).