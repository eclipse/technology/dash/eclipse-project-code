/*************************************************************************
 * Copyright (c) 2022 The Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made available under
 * the terms of the Eclipse Public License 2.0 which accompanies this
 * distribution, and is available at https://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 *************************************************************************/
package org.eclipse.dash.ip.project.code;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.eclipse.dash.api.EclipseApi;
import org.eclipse.dash.api.Project;
import org.eclipse.dash.api.Proposal;
import org.eclipse.dash.git.IRepository;
import org.eclipse.dash.git.RepositoryService;
import org.gitlab4j.api.Constants.IssueState;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Issue;
import org.gitlab4j.api.models.IssueFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.inject.Inject;

public class ReviewTaskFinder {
	final Logger logger = LoggerFactory.getLogger(ReviewTaskFinder.class);
	
	@Inject
	EclipseApi api;
	
	@Inject 
	RepositoryService repositoryService;
	
	@Inject 
	GitLabApi gitlabApi;
	
	@Inject
	ReviewTaskFinder initialContribution;
		
	public Stream<CreateReviewTask> tasks() {
		return Stream.concat(initialContributionTasks(), adHocReviewTasks());
	}
	
	Stream<CreateReviewTask> initialContributionTasks() {
		try {
			IssueFilter filter = new IssueFilter()
					.withState(IssueState.OPENED)
					.withLabels(Arrays.asList("EDP::Initial Contribution", "IP review"));
			return gitlabApi.getIssuesApi()
				.getIssuesStream(InitialContributionProcess.getEmoProjectPath(), filter)
				.map(each -> createInitialContributionTaskFromProjectId(each, getProjectId(each)))
				.filter(Objects::nonNull);
		} catch (GitLabApiException e) {
			logger.debug(e.getLocalizedMessage(), e);
		}
		return Stream.empty();
	}
	
	Stream<CreateReviewTask> adHocReviewTasks() {
		try {
			IssueFilter filter = new IssueFilter()
					.withState(IssueState.OPENED)
					.withLabels(Collections.singletonList("Initiate IP Review"));
			return gitlabApi.getIssuesApi()
				.getIssuesStream(InitialContributionProcess.getEmoProjectPath(), filter)
				.map(each -> createAdHocReviewTaskFromProjectId(each, getProjectId(each)))
				.filter(Objects::nonNull);
		} catch (GitLabApiException e) {
			logger.debug(e.getLocalizedMessage(), e);
		}
		return Stream.empty();
	}

	String getProjectId(Issue issue) {
		var projectId = getProjectIdFromProposalLink(issue);
		if (projectId != null) return projectId;
		
		projectId = getProjectIdFromProjectLink(issue);
		if (projectId != null) return projectId;
		
		projectId = getProjectIdFromIssueTitle(issue);
		if (projectId != null) return projectId;
		
		logger.warn("Could not identify project from issue #{}", issue.getIid());
		
		return null;
	}
	
	private String getProjectIdFromProjectLink(Issue issue) {
		Pattern EclipseProjectIdPattern = Pattern.compile("Project: \\[.+\\]\\(https:\\/\\/projects\\.eclipse\\.org\\/projects\\/(?<id>[\\w-]+(?:\\.[\\w-]+){0,2})\\)");
		var matcher = EclipseProjectIdPattern.matcher(issue.getDescription());
		if (matcher.find()) {
			var id = matcher.group("id");
			logger.info("Found a project ID: {}", id);
			return id;
		}
		
		return null;
	}
	
	private String getProjectIdFromIssueTitle(Issue issue) {
		return null;
	}
	
	public String getProjectIdFromProposalLink(Issue issue) {
		Pattern EclipseProposalPattern = Pattern.compile("Project proposal: \\[.+\\]\\((?<url>https:\\/\\/projects\\.eclipse\\.org\\/proposals\\/(?:[\\w-]+))\\/?\\)", Pattern.CASE_INSENSITIVE);
		var matcher = EclipseProposalPattern.matcher(issue.getDescription());
		if (matcher.find()){
			var url = matcher.group("url");
			logger.info("Found a project proposal URL: {}", url);
			Proposal proposal = api.getProposal(url);
			if (proposal.exists()) {
				var id = proposal.getProjectId();
				return id;
			} else {
				logger.warn("Could not find a proposal with URL {}", url);
			}
		}
		return null;
	}

	private CreateReviewTask createInitialContributionTaskFromProjectId(Issue issue, String id) {
		if (id == null) return null;
		
		// FIXME Temporary Hack
		if ("automotive.tractusx".equals(id)) return null;
		if ("ecd.opensmartclide".equals(id)) return null;
		if ("dt.aas4j".equals(id)) return null;
		
		if (id != null) {
			var project = api.getProject(id);
			if (project.exists()) {
				logger.info("Creating Initial Contribution review task for project: {}", project.getName());
				return new CreateInitialContributionReviewTask(issue, project);
			} else {
				logger.warn("A project with id {} does not exist", id);
			}
		}
		
		return null;
	}

	private CreateReviewTask createAdHocReviewTaskFromProjectId(Issue issue, String id) {
		if (id == null) return null;
		
		if (id != null) {
			var project = api.getProject(id);
			if (project.exists()) {
				logger.info("Creating Ad Hoc review task for project: {}", project.getName());
				return new CreateAdHocReviewTask(issue, project);
			} else {
				logger.warn("A project with id {} does not exist", id);
			}
		}
		
		return null;
	}
	
	public abstract class CreateReviewTask {
		Issue issue;
		Project project;
		
		public CreateReviewTask(Issue issue, Project project) {
			this.issue = issue;
			this.project = project;
		}
		
		abstract void execute();
	}
	
	class CreateInitialContributionReviewTask extends CreateReviewTask {
		
		public CreateInitialContributionReviewTask(Issue issue, Project project) {
			super(issue, project);
		}

		@Override
		public void execute() {
			repositoryService.repositories(project)
				.limit(InitialContributionProcess.getReviewCreationLimit())
				.forEach(each -> new CreateInitialContributionReview(issue, project, each).createAndLinkReview());
		}
	}
	
	
	class CreateInitialContributionReview extends CreateRepositoryReview {

		public CreateInitialContributionReview(Issue issue, Project project, IRepository repository) {
			super(issue, project, repository);
		}
		

		public void createAndLinkReview() {
			logger.info("Setting up an initial contribution review for {}", repository.getWebUrl());
			var review = getExisting();
			if (review == null) {
				if (!repository.isEmpty()) {
					if (repository.hasPotentiallyInterestingContent()) {
						review = createReview();
					} else {
						logger.info("Did not create a review; repository doesn't look interesting {}", repository.getWebUrl());
						return;
					}
				} else {
					logger.info("Did not create a review; repository is empty {}", repository.getWebUrl());
					return;
				}
			} else {
				logger.info("An issue already exists {}", review.getWebUrl());
			}
			
			linkReview(review);
		}
		
		String getLabels() {
			return super.getLabels() + ",Initial Contribution";
		}
		
		Issue getExisting() {
			var match = "project/{project}/{path}/{name}/"
					.replace("{project}", project.getId())
					.replace("{path}", repository.getNamespace())
					.replace("{name}", repository.getName());
			
			try {
				IssueFilter filter = new IssueFilter().withLabels(Arrays.asList(getSearchLabels()));
				return gitlabApi.getIssuesApi()
						.getIssuesStream(InitialContributionProcess.getIPLabPath(), filter)
						.filter(issue -> issue.getTitle().startsWith(match))
						.findAny().orElse(null);
			} catch (GitLabApiException e) {
				throw new RuntimeException(e);
			}
		}

		String[] getSearchLabels() {
			return new String[]{"Project Content","Initial Contribution"};
		}
	}
	
	abstract class CreateRepositoryReview {

		Issue issue;
		Project project;
		IRepository repository;

		public CreateRepositoryReview(Issue issue, Project project, IRepository repository) {
			this.issue = issue;
			this.project = project;
			this.repository = repository;
		}

		public void createAndLinkReview() {
			logger.info("Setting up review for {}", repository.getWebUrl());
			if (!repository.isEmpty()) {
				if (repository.hasPotentiallyInterestingContent()) {
					Issue review = createReview();
					linkReview(review);
				} else {
					logger.info("Did not create a review; repository doesn't look interesting {}", repository.getWebUrl());
				}
			} else {
				logger.info("Did not create a review; repository is empty {}", repository.getWebUrl());
				return;
			}
		}

		void linkReview(Issue review) {
			try {
				var linkExists = gitlabApi.getIssuesApi().getIssueLinksStream(InitialContributionProcess.getIPLabPath(), review.getIid())
						.filter(each -> each.getIid().equals(issue.getIid()))
						.findAny()
						.isPresent();
				
				if (!linkExists) {
					gitlabApi.getIssuesApi().createIssueLink(InitialContributionProcess.getIPLabPath(), review.getIid(), InitialContributionProcess.getEmoProjectPath(), issue.getIid());
				}
			} catch (GitLabApiException e) {
				// NOTE that the GitLab user must have privileges on both repositories
				// for the link creation to work.
				throw new RuntimeException(e);
			}
		}

		Issue createReview() {
			var title = getTitle(repository);
			var description = getDescription(repository);
			var labels = getLabels();
			
			try {
				Issue review = gitlabApi.getIssuesApi().createIssue(InitialContributionProcess.getIPLabPath(), title, description, null, null, null, labels, null, null, null, null);
				logger.info("Review created {}", review.getWebUrl());
				return review;
			} catch (GitLabApiException e) {
				if (e.getHttpStatus() == 429) {
					logger.info("Too many requests. Let's try that again in a few minutes (current time: {})", DateTimeFormatter.ofPattern("HH:mm:ss").format(LocalDateTime.now()));
					try {
						Thread.sleep(2 * 60 * 1000);
					} catch (InterruptedException e1) {
					}
					return createReview();
				}
				throw new RuntimeException(e);
			}
		}

		String getLabels() {
			return "Project Content,Review Needed";
		}

		String getTitle(IRepository repository) {
			return "project/{project}/{path}/{name}/{sha}"
					.replace("{project}", project.getId())
					.replace("{path}", repository.getNamespace())
					.replace("{name}", repository.getName())
					.replace("{sha}", repository.getTopCommitSha());
		}
		
		String getDescription(IRepository repository) {
			var browseUrl = repository.getBrowseUrl();
			var archiveUrl = repository.getArchiveUrl();
			var license = repository.getLicense();
			if (license == null) license = "N/A";
			
			var builder = new StringBuilder();
			return builder
					.append("Project: ").append(getProjectLink()).append("\n\n")
					//.append(" - License: {license}\n".replace("{license}", project.getLicense().toString()))
					
					.append("[Repository]({url})\n\n".replace("{url}", repository.getWebUrl()))
					.append(" - Default branch: {branch}\n".replace("{branch}", repository.getDefaultBranch()))
					.append(" - Commit: {sha}\n".replace("{sha}", repository.getTopCommitSha()))
					.append(" - License: {license}\n".replace("{license}", license))
					.append(" - [Browse]({url})\n".replace("{url}", browseUrl))
					.append(" - [Source]({url})\n".replace("{url}", archiveUrl))
					.toString();
		}		
		
		String getProjectLink() {
			return "[{name}]({url})".replace("{name}", project.getName()).replace("{url}", project.getUrl());
		}
	}
	
	class CreateAdHocReviewTask extends CreateReviewTask {

		CreateAdHocReviewTask(Issue issue, Project project) {
			super(issue, project);
		}

		@Override
		void execute() {
			if (removeReviewRequestLabel() ) {
				repositoryService.repositories(project)
				.limit(InitialContributionProcess.getReviewCreationLimit())
				.forEach(each -> new CreateAdHocReview(issue, project, each).createAndLinkReview());
			}
		}

		private boolean removeReviewRequestLabel() {
			var labels = new HashSet<String>();
			labels.addAll(issue.getLabels());
			var labelExists = labels.remove("Initiate IP Review");
			
			try {
				gitlabApi.getIssuesApi().updateIssue(InitialContributionProcess.getEmoProjectPath(), issue.getIid(), null, null, null, null, null,
						String.join(",", labels), null, null, null);
			} catch (GitLabApiException e) {
				throw new RuntimeException(e);
			}
			
			return labelExists;
		}
	}
	
	class CreateAdHocReview extends CreateRepositoryReview {

		public CreateAdHocReview(Issue issue, Project project, IRepository repository) {
			super(issue, project, repository);
		}
		

		public void createAndLinkReview() {
			logger.info("Setting up an ad hoc review for {}", repository.getWebUrl());
			var review = getExisting();
			if (review == null) {
				if (!repository.isEmpty()) {
					if (repository.hasPotentiallyInterestingContent()) {
						review = createReview();
					} else {
						logger.info("Did not create a review; repository doesn't look interesting {}", repository.getWebUrl());
					}
				} else {
					logger.info("Did not create a review; repository is empty {}", repository.getWebUrl());
					return;
				}
			} else {
				logger.info("An issue already exists {}", review.getWebUrl());
			}
			
			linkReview(review);
		}
		
		Issue getExisting() {
			var match = "project/{project}/{path}/{name}/{sha}"
					.replace("{project}", project.getId())
					.replace("{path}", repository.getNamespace())
					.replace("{name}", repository.getName())
					.replace("{sha}", repository.getTopCommitSha());
			
			try {
				IssueFilter filter = new IssueFilter().withLabels(Arrays.asList(new String[]{"Project Content", "Initial Contribution"}));
				return gitlabApi.getIssuesApi()
						.getIssuesStream(InitialContributionProcess.getIPLabPath(), filter)
						.filter(issue -> issue.getTitle().equals(match))
						.findAny().orElse(null);
			} catch (GitLabApiException e) {
				throw new RuntimeException(e);
			}
		}
	}
}
