/*************************************************************************
 * Copyright (c) 2022 The Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made available under
 * the terms of the Eclipse Public License 2.0 which accompanies this
 * distribution, and is available at https://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 *************************************************************************/
package org.eclipse.dash.ip.project.code;

import java.util.function.Consumer;

import org.eclipse.dash.ip.project.code.context.ProjectCodeReviewModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class InitialContributionProcess {
	final Logger logger = LoggerFactory.getLogger(InitialContributionProcess.class);
	

	public static void main(String[] args) throws InterruptedException {
		new InitialContributionProcess().run();
	}
	
	public void run() {
		run((provider) -> {
			provider.tasks().forEach(task -> task.execute());
		});
	}
	
	private void run(Consumer<ReviewTaskFinder> doit) {
		logger.info("Starting the processor.");
		
		Injector injector = Guice.createInjector(new ProjectCodeReviewModule());
		var scanner = injector.getInstance(ReviewTaskFinder.class);
		
		doit.accept(scanner);
	}
	
	static String getEmoProjectPath() {
		return System.getProperty("org.eclipse.dash.emo-path", "eclipsefdn/emo-team/emo");
	}
	
	static String getIPLabPath() {
		return System.getProperty("org.eclipse.dash.iplab-path", "eclipsefdn/emo-team/iplab");
	}	
	
	public static long getReviewCreationLimit() {
		try {
			return Long.valueOf(System.getProperty("org.eclipse.dash.limit"));
		} catch (NumberFormatException e) {
			return 1000;
		}
	}
}
