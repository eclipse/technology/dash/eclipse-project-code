package org.eclipse.dash.ip.project.code;

public class ReviewCreationException extends RuntimeException {

	public ReviewCreationException(String string) {
		super(string);
	}

	private static final long serialVersionUID = 6349689561101909874L;

}
