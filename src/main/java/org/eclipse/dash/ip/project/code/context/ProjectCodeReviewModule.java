/*************************************************************************
 * Copyright (c) 2022 The Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made available under
 * the terms of the Eclipse Public License 2.0 which accompanies this
 * distribution, and is available at https://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 *************************************************************************/
package org.eclipse.dash.ip.project.code.context;

import java.io.IOException;
import java.io.InputStream;
import java.util.function.Consumer;

import org.eclipse.dash.api.EclipseApi;
import org.eclipse.dash.api.EclipseApi.HttpService;
import org.eclipse.dash.git.RepositoryService;
import org.eclipse.dash.ip.project.code.ReviewTaskFinder;
import org.eclipse.dash.licenses.IProxySettings;
import org.eclipse.dash.licenses.ISettings;
import org.eclipse.dash.licenses.http.HttpClientService;
import org.eclipse.dash.licenses.http.IHttpClientService;
import org.gitlab4j.api.GitLabApi;
import org.kohsuke.github.GitHub;

import com.google.inject.AbstractModule;
import com.google.inject.util.Providers;

public class ProjectCodeReviewModule extends AbstractModule {

	@Override
	protected void configure() {
		bind(ISettings.class).toInstance(new ISettings() {
			@Override
			public int getTimeout() {
				// TODO Auto-generated method stub
				return 5 * 60;
			}
		});
		bind(GitLabApi.class).toInstance(new GitLabApi(getGitLabUrl(), getIpLabToken().trim()));
		
		HttpClientService httpClient = new HttpClientService();
		
		bind(EclipseApi.class).toInstance(new EclipseApi(new HttpService() {
			@Override
			public int get(String url, String contentType, Consumer<InputStream> handler) {
				return httpClient.get(url, contentType, handler);
			}
		}));
		bind(IHttpClientService.class).toInstance(httpClient);		
		bind(IProxySettings.class).toProvider(Providers.of(null));
		
		try {
			var github = GitHub.connect();
			github.checkApiUrlValidity();
			bind(GitHub.class).toInstance(github);
		} catch (IOException e) {
			throw new RuntimeException("GitHub connection not valid (check GITHUB_LOGIN and GITHUB_OAUTH).", e);
		}
		
		bind(RepositoryService.class).toInstance(new RepositoryService());
		
		bind(ReviewTaskFinder.class).toInstance(new ReviewTaskFinder());
	}
	
	static String getGitLabUrl() {
		return System.getProperty("org.eclipse.dash.gitlab", "https://gitlab.eclipse.org");
	}
	
	static String getIpLabToken() {
		return System.getProperty("org.eclipse.dash.token", "");
	}
}
