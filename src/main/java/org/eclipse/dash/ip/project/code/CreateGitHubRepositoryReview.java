package org.eclipse.dash.ip.project.code;

import java.io.IOException;
import java.util.Arrays;

import org.eclipse.dash.api.Project;
import org.eclipse.dash.api.SourceRepository;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Issue;
import org.gitlab4j.api.models.IssueFilter;
import org.kohsuke.github.GHRepository;
import org.kohsuke.github.GitHub;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class CreateGitHubRepositoryReview {

	final Logger logger = LoggerFactory.getLogger(CreateGitHubRepositoryReview.class);
	private Issue issue;
	private Project project;
	private GitLabApi gitlab;
	private GitHub github;
	private SourceRepository repository;

	public CreateGitHubRepositoryReview(GitLabApi gitlab, Issue issue, Project project, SourceRepository repository, GitHub github) {
		this.gitlab = gitlab;
		this.issue = issue;
		this.project = project;
		this.repository = repository;
		this.github = github;
	}

	public void createReview() {
		logger.info("Setting up review for {}", repository.getUrl());
		
		var existing = getExisting(repository);
		if (existing == null) {
			try {
				var repo = github.getRepository(repository.getPath() + "/" + repository.getName());
				
				if (repo.isArchived()) {
					logger.info("Skipping archived repository {}", repo.getUrl());
					return;
				}
				
				
				if (repo.getBranches().isEmpty()) {
					logger.info("Skipping empty repository {}", repo.getUrl());
					return;
				}
				
				var title = getTitle(repo);
				var description = getDescription(repo);
				var labels = "Project Content,Initial Contribution,Review Needed";
				
				try {
					existing = gitlab.getIssuesApi().createIssue(InitialContributionProcess.getIPLabPath(), title, description, null, null, null, labels, null, null, null, null);
				} catch (GitLabApiException e) {
					throw new RuntimeException(e);
				}
				logger.info("Review created {}", existing.getWebUrl());
			} catch (IOException e1) {
				throw new RuntimeException(e1);
			}
		} else {
			logger.info("An issue already exists {}", existing.getWebUrl());
		}
		
		try {
			var linkExists = gitlab.getIssuesApi().getIssueLinksStream(InitialContributionProcess.getIPLabPath(), existing.getIid())
					.filter(each -> each.getIid().equals(issue.getIid()))
					.findAny()
					.isPresent();
			
			if (!linkExists) {
				gitlab.getIssuesApi().createIssueLink(InitialContributionProcess.getIPLabPath(), existing.getIid(), InitialContributionProcess.getEmoProjectPath(), issue.getIid());
			}
		} catch (GitLabApiException e) {
			// NOTE that the GitLab user must have privileges on both repositories
			// for the link creation to work.
			throw new RuntimeException(e);
		}
	}

	private Issue getExisting(SourceRepository repository) {
		var match = "project/{project}/{path}/{name}/"
				.replace("{project}", project.getId())
				.replace("{path}", repository.getPath())
				.replace("{name}", repository.getName());
		
		try {
			IssueFilter filter = new IssueFilter().withLabels(Arrays.asList(new String[]{"Project Content","Initial Contribution"}));
			return gitlab.getIssuesApi()
					.getIssuesStream(InitialContributionProcess.getIPLabPath(), filter)
					.filter(issue -> issue.getTitle().startsWith(match))
					.findAny().orElse(null);
		} catch (GitLabApiException e) {
			throw new RuntimeException(e);
		}
	}
	
	private String getTitle(GHRepository repository) throws IOException {
		return "project/{source}/{name}/{sha}"
				.replace("{source}", project.getId())
				.replace("{name}", repository.getFullName())
				.replace("{sha}", repository.getBranch(repository.getDefaultBranch()).getSHA1());
	}
	
	private String getDescription(GHRepository repo) throws IOException {
		String sha = repo.getBranch(repo.getDefaultBranch()).getSHA1();
		
		var browseUrl = "https://github.com/{name}/tree/{sha}"
				.replace("{name}", repo.getFullName())
				.replace("{branch}", repo.getDefaultBranch())
				.replace("{sha}", sha);
		
		var archiveUrl = "https://github.com/{name}/archive/{sha}.zip"
				.replace("{name}", repo.getFullName())
				.replace("{sha}", sha);
		
		var license = repo.getLicense();
		var licenseName = license != null? license.getName() : "N/A";
		
		var builder = new StringBuilder();
		return builder
				.append("Project: ").append(getProjectLink()).append("\n\n")
				//.append(" - License: {license}\n".replace("{license}", project.getLicense().toString()))
				
				.append("[Repository]({url})\n\n".replace("{url}", repo.getHtmlUrl().toString()))
				.append(" - Default branch: {branch}\n".replace("{branch}", repo.getDefaultBranch()))
				.append(" - Commit: {sha}\n".replace("{sha}", sha))
				.append(" - License: {license}\n".replace("{license}", licenseName))
				.append(" - [Browse]({url})\n".replace("{url}", browseUrl))
				.append(" - [Source]({url})\n".replace("{url}", archiveUrl))
				.toString();
	}
	
	private String getProjectLink() {
		return "[{name}]({url})".replace("{name}", project.getName()).replace("{url}", project.getUrl());
	}
}