/*************************************************************************
 * Copyright (c) 2023 The Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made available under
 * the terms of the Eclipse Public License 2.0 which accompanies this
 * distribution, and is available at https://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 *************************************************************************/
package org.eclipse.dash.git;

import java.util.regex.Pattern;
import java.util.stream.Stream;

import jakarta.inject.Inject;

import org.eclipse.dash.api.EclipseApi;
import org.eclipse.dash.api.Project;

public class RepositoryService {
	@Inject
	EclipseApi api;
	@Inject
	GitLabService gitlabService;
	
	@Inject
	GitHubService gitHubService;
	
	public Stream<IRepository> repositories(Project project) {
		return Stream.concat(gitlabRepositories(project), githubRepositories(project))
				.filter(each -> !each.getName().contains("website"))
				.filter(each -> !each.isMetadata())
				.filter(each -> !each.isArchived())
				.filter(each -> isProjectRepository(project, each))
				.distinct();
	}

	Stream<IRepository> githubRepositories(Project project) {
		return Stream.concat(githubRepositoriesInOrg(project), githubRepositoriesListed(project));
	}

	Stream<IRepository> githubRepositoriesInOrg(Project project) {
		return gitHubService.getRepositories(api.getRepositoriesApi().getGitHubOrg(project));
	}

	Stream<IRepository> githubRepositoriesListed(Project project) {
		return api.getRepositoriesApi().githubRepositories(project)
				.map(each -> gitHubService.getRepository(each));
	}

	Stream<IRepository> gitlabRepositories(Project project) {
		return gitlabService.getRepositories(api.getRepositoriesApi().gitlabProjectGroup(project));
	}

	/**
	 * This is a hack to skip over repositories that we know are not project code
	 * despite being referenced in the project metadata. We know, for example, that
	 * the Eclipse Adoptium projects have working clones of JDK content. We want to
	 * avoid including these repositories in our work.
	 * 
	 * @param project
	 * @param each
	 * @return
	 */
	boolean isProjectRepository(Project project, IRepository each) {
		switch (project.getId()) {
			case "adoptium.temurin" :
				if (Pattern.matches(".*/jdk$", each.getWebUrl())) return false;
				if (Pattern.matches(".*jdk\\d+u$", each.getWebUrl())) return false;
				//if (Pattern.matches(".*-binaries$", each.getWebUrl())) return false;
			default:
		}
		return true;
	}
}
