/*************************************************************************
 * Copyright (c) 2023 The Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made available under
 * the terms of the Eclipse Public License 2.0 which accompanies this
 * distribution, and is available at https://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 *************************************************************************/
package org.eclipse.dash.git;

import java.io.IOException;
import java.util.stream.Stream;

import jakarta.inject.Inject;

import org.eclipse.dash.api.SourceRepository;
import org.kohsuke.github.GHContent;
import org.kohsuke.github.GHLicense;
import org.kohsuke.github.GHRepository;
import org.kohsuke.github.GitHub;

public class GitHubService {
	@Inject GitHub github;
	
	public IRepository getRepository(SourceRepository repositoryData) {
		try {
			var repository = github.getRepository(repositoryData.getPath() + "/" + repositoryData.getName());
			return new GitHubRepo(repository);
		} catch (IOException e) {
			throw new RuntimeException("Error while trying to get the repositories from the org.", e);
		}
	}
	
	public Stream<IRepository> getRepositories(String org) {
		if (org == null || org.isEmpty()) return Stream.empty();
		try {
			return github.getOrganization(org).getRepositories().values().stream()
					.map(each -> new GitHubRepo(each));
		} catch (IOException e) {
			throw new RuntimeException("Error while trying to get the repositories from the org.", e);
		}
	}
	
	public class GitHubRepo implements IRepository {


		private GHRepository repository;

		public GitHubRepo(GHRepository repository) {
			this.repository = repository;
		}

		@Override
		public String getId() {
			return "github";
		}
		
		@Override
		public String getWebUrl() {
			return repository.getHtmlUrl().toString();
		}

		@Override
		public boolean isEmpty() {
			try {
				if (repository.getBranches().isEmpty()) return true;
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
			return false;
		}

		@Override
		public String getDefaultBranch() {
			return repository.getDefaultBranch();
		}

		@Override
		public String getNamespace() {
			return repository.getFullName().split("/")[0];
		}

		@Override
		public String getName() {
			return repository.getFullName().split("/")[1];
		}

		@Override
		public String getTopCommitSha() {
			try {
				return repository.getBranch(repository.getDefaultBranch()).getSHA1();
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}

		@Override
		public String getBrowseUrl() {
			return "https://github.com/{name}/tree/{sha}"
					.replace("{name}", repository.getFullName())
					.replace("{branch}", repository.getDefaultBranch())
					.replace("{sha}", getTopCommitSha());
		}

		@Override
		public String getArchiveUrl() {
			return "https://github.com/{name}/archive/{sha}.zip"
					.replace("{name}", repository.getFullName())
					.replace("{sha}", getTopCommitSha());
		}

		@Override
		public String getLicense() {
			try {
				GHLicense license = repository.getLicense();
				return license != null ? license.getName() : null;
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}

		@Override
		public boolean isMetadata() {
			switch (getName()) {
				case ".eclipsefdn": 
				case ".gitHub":
				case ".github":
					return true;
			}
			return false;
		}

		@Override
		public boolean isArchived() {
			if ("eclipse-threadx".equals(getNamespace())) return false;
			return repository.isArchived();
		}
		
		@Override
		public boolean hasPotentiallyInterestingContent() {
			if (isEmpty()) return false;
			
			try {
				return repository.getDirectoryContent(".").stream().anyMatch(each -> isPotentiallyInterestingContent(each));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
		}
		
		boolean isPotentiallyInterestingContent(GHContent content) {			
			if (content.isDirectory()) {
				return isPotentiallyInterestingDirectory(content);
			} else {
				return isPotentiallyInterestingFile(content);
			}
		}

		/**
		 * This method answers whether or not the file is potentially interesting. Any
		 * file with a name that starts with a dot or is entirely upper case (except in
		 * the file extension) is assumed to be a metadata file (e.g., "README.md") and
		 * not considered interesting. Everything else is potentially interesting.
		 */
		boolean isPotentiallyInterestingFile(GHContent content) {
			var name = content.getName();
			if (name.startsWith(".")) return false;
			
			var last = name.lastIndexOf('.');
			if (last > 0) name = name.substring(0, last);
			
			return !name.chars().allMatch(each -> !Character.isLowerCase(each));
		}

		boolean isPotentiallyInterestingDirectory(GHContent content) {
			String name = content.getName();
			
			if (name.startsWith(".")) return false;
			
			if (name.equals("otterdog")) return false;
			
			return true;
		}
		
		@Override
		public boolean equals(Object obj) {
			if (this == obj) return true;
			if (!(obj instanceof GitHubRepo)) return false;
			
			return repository.equals(((GitHubRepo)obj).repository);
		}
		
		@Override
		public int hashCode() {
			return repository.hashCode();
		}
	}
}
