/*************************************************************************
 * Copyright (c) 2023 The Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made available under
 * the terms of the Eclipse Public License 2.0 which accompanies this
 * distribution, and is available at https://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 *************************************************************************/
package org.eclipse.dash.git;

public interface IRepository {

	String getWebUrl();

	boolean isEmpty();

	String getDefaultBranch();

	String getNamespace();

	String getName();

	String getTopCommitSha();

	String getBrowseUrl();

	String getArchiveUrl();

	String getLicense();

	boolean isMetadata();

	boolean isArchived();

	default boolean hasPotentiallyInterestingContent() {
		return false;
	}

	String getId();

}
