/*************************************************************************
 * Copyright (c) 2023 The Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made available under
 * the terms of the Eclipse Public License 2.0 which accompanies this
 * distribution, and is available at https://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 *************************************************************************/
package org.eclipse.dash.git;

import java.util.stream.Stream;

import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Project;
import org.gitlab4j.api.models.TreeItem;

import jakarta.inject.Inject;

public class GitLabService {
	@Inject GitLabApi api;
	
	public Stream<IRepository> getRepositories(String gitlabProjectGroup) {
		if (gitlabProjectGroup == null || gitlabProjectGroup.isEmpty()) return Stream.empty();
		return getProjects(gitlabProjectGroup).map(each -> new GitlabRepository(each));
	}
	
	public Stream<Project> getProjects(String rootPath) {
		if (rootPath == null || rootPath.isEmpty()) return Stream.empty();
		return getGroups(rootPath)
				.flatMap(each -> {
					try {
						return api.getGroupApi().getProjects(each).stream();
					} catch (GitLabApiException e) {
						throw new RuntimeException(e);
					}
				});
	}
	
	public Stream<String> getGroups(String path) {
		Stream<String> subgroups;
		try {
			subgroups = api.getGroupApi().getSubGroups(path, null, true, null, null, null, null, null).stream()
					.map(each -> each.getFullPath());
			return Stream.concat(Stream.of(path),subgroups.flatMap(each -> getGroups(each)));
		} catch (GitLabApiException e) {
			throw new RuntimeException(e);
		}
	}
	
	public class GitlabRepository implements IRepository {

		private Project repository;

		public GitlabRepository(Project repository) {
			this.repository = repository;
		}

		@Override
		public String getId() {
			return "gitlab";
		}

		@Override
		public String getWebUrl() {
			return repository.getWebUrl();
		}

		@Override
		public boolean isEmpty() {
			try {
				return api.getRepositoryApi().getBranches(repository).isEmpty();
			} catch (GitLabApiException e) {
				throw new RuntimeException(e);
			}
		}
		
		@Override
		public boolean hasPotentiallyInterestingContent() {
			if (isEmpty()) return false;
			
			try {
				return api.getRepositoryApi().getTree(repository.getId(), ".", null).stream().anyMatch(each -> isPotentiallyInterestingFile(each));
			} catch (GitLabApiException e) {
				throw new RuntimeException(e);
			}
		}
		
		boolean isPotentiallyInterestingFile(TreeItem content) {
			var name = content.getName();
			System.out.println(name);
			if (name.startsWith(".")) return false;
			
			var last = name.lastIndexOf('.');
			if (last > 0) name = name.substring(0, last);
			
			return !name.chars().allMatch(each -> !Character.isLowerCase(each));
		}

		@Override
		public String getDefaultBranch() {
			return repository.getDefaultBranch();
		}

		@Override
		public String getNamespace() {
			return repository.getNamespace().getFullPath().replace("/","-");
		}

		@Override
		public String getName() {
			return repository.getPath();
		}

		@Override
		public String getTopCommitSha() {
			try {
				var branch = api.getRepositoryApi().getBranch(repository, getDefaultBranch());
				return branch.getCommit().getId();
			} catch (GitLabApiException e) {
				throw new RuntimeException(e);
			}
		}

		@Override
		public String getBrowseUrl() {
			return "https://gitlab.eclipse.org/{path}/{name}/-/commit/{sha}"
					.replace("{path}", repository.getNamespace().getFullPath())
					.replace("{name}", repository.getPath())
					.replace("{sha}", getTopCommitSha());
		}

		@Override
		public String getArchiveUrl() {
			return "https://gitlab.eclipse.org/api/v4/projects/{path}%2F{name}/repository/archive.zip?sha={sha}"
					.replace("{path}", repository.getNamespace().getFullPath().replace("/", "%2F"))
					.replace("{name}", repository.getPath())
					.replace("{sha}", getTopCommitSha());
		}

		@Override
		public String getLicense() {
			var license = repository.getLicense();
			return license != null ? license.getName() : null;
		}

		@Override
		public boolean isMetadata() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean isArchived() {
			return repository.getArchived();
		}
		
		@Override
		public boolean equals(Object obj) {
			if (this == obj) return true;
			if (!(obj instanceof GitlabRepository)) return false;
			
			return repository.equals(((GitlabRepository)obj).repository);
		}
		
		@Override
		public int hashCode() {
			return repository.hashCode();
		}
	}
}
