/*************************************************************************
 * Copyright (c) 2024 The Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made available under
 * the terms of the Eclipse Public License 2.0 which accompanies this
 * distribution, and is available at https://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 *************************************************************************/
package org.eclipse.dash.iplab.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.eclipse.dash.api.Project;
import org.eclipse.dash.git.IRepository;
import org.eclipse.dash.ip.project.code.ReviewCreationException;
import org.gitlab4j.api.Constants.IssueState;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Issue;
import org.gitlab4j.api.models.IssueFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.inject.Inject;

public class ReviewCreationService {
	final Logger logger = LoggerFactory.getLogger(ReviewCreationService.class);
		
	@Inject 
	GitLabApi gitlabApi;
	
	public static String PROJECT_CONTENT = "Project Content";
	public static String INITIAL_CONTRIBUTION = "Initial Contribution";
	public static String REVIEW_NEEDED = "Review Needed";

	private String iplabPath;
	
	public ReviewCreationService(String iplabPath) {
		this.iplabPath = iplabPath;
	}
	
	@Inject
	public void validate() {
		try {
			var project = gitlabApi.getProjectApi().getProject(iplabPath);
			logger.info("IPLab project: {}", project.getName());
		} catch (GitLabApiException e) {
			throw new RuntimeException(e);
		}
	}
	
	public Issue findExistingReview(String[] labels, boolean includeClosed, Predicate<Issue> test) throws GitLabApiException {
		IssueFilter filter = new IssueFilter().withLabels(Arrays.asList(labels));
		if (!includeClosed) 
			filter = filter.withState(IssueState.OPENED);
		
		return gitlabApi.getIssuesApi()
				.getIssuesStream(iplabPath, filter)
				.filter(test)
				.findAny().orElse(null);
	}

	String[] getSearchLabels() {
		return new String[]{"Project Content","Initial Contribution"};
	}
	
	public void createReview(Project project, IRepository repository, String[] labels) throws GitLabApiException {
		logger.info("Setting up review for {}", repository.getBrowseUrl());
		
				
		if (repository.isArchived()) {
			logger.info("Skipping archived repository {}", repository.getBrowseUrl());
			return;
		}
		
		if (repository.getDefaultBranch() == null) {
			logger.info("Skipping empty repository {}", repository.getBrowseUrl());
			return;
		}
		
		var title = getTitle(repository);
		var description = getDescription(repository, project);
		var labelsString = Arrays.stream(labels).collect(Collectors.joining(","));
		
		Issue issue = null;
		for(int count=0;count<10;count++) {
			try {
				issue = gitlabApi.getIssuesApi().createIssue(iplabPath, title, description, null, null, null, labelsString, null, null, null, null);
				break;
			} catch (GitLabApiException e) {
				if (e.getHttpStatus() == 429) {
					logger.info("Too many requests. Let's try that again in a few minutes (current time: {})", DateTimeFormatter.ofPattern("HH:mm:ss").format(LocalDateTime.now()));
					try {
						Thread.sleep(2 * 60 * 1000);
					} catch (InterruptedException e1) {
					}
				}
			}
		}
		if (issue == null) {
			logger.error("Failed to create a review");
			throw new ReviewCreationException("Failed to create a review");
		}
		
		logger.info("Review created {}", issue.getWebUrl());
	}
		
	private String getTitle(IRepository repository) {
		return "{type}/{source}/{namespace}/{name}/{sha}"
				.replace("{type}", "git")
				.replace("{source}", repository.getId())
				.replace("{namespace}", repository.getNamespace())
				.replace("{name}", repository.getName())
				.replace("{sha}", repository.getTopCommitSha());
	}
	
	private String getDescription(IRepository repository, Project project) {
		var license = repository.getLicense();
		
		var builder = new StringBuilder();
		return builder
			.append("Project: ").append(getProjectLink(project)).append("\n\n")
			//.append(" - License: {license}\n".replace("{license}", project.getLicense().toString()))
			
			.append("[Repository]({url})\n\n".replace("{url}", repository.getBrowseUrl()))
			.append(" - Default branch: {branch}\n".replace("{branch}", repository.getDefaultBranch()))
			.append(" - Commit: {sha}\n".replace("{sha}", repository.getTopCommitSha()))
			.append(" - License: {license}\n".replace("{license}", license == null || license.isBlank() ? "N/A" : license))
			.append(" - [Browse]({url})\n".replace("{url}", repository.getBrowseUrl()))
			.append(" - [Source]({url})\n".replace("{url}", repository.getArchiveUrl()))
			.toString();
	}
	
	private String getProjectLink(Project project) {
		return "[{name}]({url})".replace("{name}", project.getName()).replace("{url}", project.getUrl());
	}
}