package org.eclipse.dash.iplab.service;

import java.util.Arrays;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.eclipse.dash.api.EclipseApi;
import org.eclipse.dash.api.Project;
import org.eclipse.dash.api.Proposal;
import org.eclipse.dash.git.IRepository;
import org.eclipse.dash.git.RepositoryService;
import org.eclipse.dash.ip.project.code.InitialContributionProcess;
import org.gitlab4j.api.Constants.IssueState;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Issue;
import org.gitlab4j.api.models.IssueFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.inject.Inject;

public class EmoTasksService {


	final Logger logger = LoggerFactory.getLogger(EmoTasksService.class);
	
	public static final String INITIAL_CONTRIBUTION = "EDP::Initial Contribution";
	public static final String IP_REVIEW = "IP review";
	
	private String emoPath;

	@Inject
	GitLabApi gitlabApi;
	
	@Inject
	EclipseApi eclipseApi;
	
	@Inject
	RepositoryService repositoryService;
	
	public EmoTasksService(String emoPath) {
		this.emoPath = emoPath;
	}

	@Inject
	public void validate() {
		try {
			var project = gitlabApi.getProjectApi().getProject(emoPath);
			logger.info("EMO project: {}", project.getName());
		} catch (GitLabApiException e) {
			throw new RuntimeException(e);
		}
	}
	
	Stream<ProjectReview> initialContributionTasks() {
		try {
			IssueFilter filter = new IssueFilter()
					.withState(IssueState.OPENED)
					.withLabels(Arrays.asList(INITIAL_CONTRIBUTION, IP_REVIEW));
			return gitlabApi.getIssuesApi()
				.getIssuesStream(emoPath, filter)
				.map(each -> new ProjectReview(each))
				.filter(each -> each.isValid());
		} catch (GitLabApiException e) {
			logger.debug(e.getLocalizedMessage(), e);
		}
		return Stream.empty();
	}
	
	class ProjectReview {
		
		private Issue issue;
		private Project project;

		public ProjectReview(Issue issue) {
			this.issue = issue;
			var projectId = getProjectId();
			if (projectId != null)
				project = eclipseApi.getProject(projectId);
		}

		public boolean isValid() {
			return project != null;
		}
		
		
		
		public void createReviews() {
			repositoryService.repositories(project)
				.limit(10)
				.forEach(each -> createAndLinkReview(each));
		}
		
		private void createAndLinkReview(IRepository each) {
		}

		private String getProjectId() {
			var projectId = getProjectIdFromProposalLink();
			if (projectId != null) return projectId;
			
			projectId = getProjectIdFromProjectLink();
			if (projectId != null) return projectId;
			
			projectId = getProjectIdFromIssueTitle();
			if (projectId != null) return projectId;
			
			logger.warn("Could not identify project from issue #{}", issue.getIid());
			
			return null;
		}
		
		private String getProjectIdFromProposalLink() {
			Pattern EclipseProposalPattern = Pattern.compile("Project proposal: \\[.+\\]\\((?<url>https:\\/\\/projects\\.eclipse\\.org\\/proposals\\/(?:[\\w-]+))\\)", Pattern.CASE_INSENSITIVE);
			var matcher = EclipseProposalPattern.matcher(issue.getDescription());
			if (matcher.find()){
				var url = matcher.group("url");
				logger.info("Found a project proposal URL: {}", url);
				Proposal proposal = eclipseApi.getProposal(url);
				if (proposal.exists()) {
					var id = proposal.getProjectId();
					return id;
				} else {
					logger.warn("Could not find a proposal with URL {}", url);
				}
			}
			return null;
		}
		
		private String getProjectIdFromProjectLink() {
			Pattern EclipseProjectIdPattern = Pattern.compile("Project: \\[.+\\]\\(https:\\/\\/projects\\.eclipse\\.org\\/projects\\/(?<id>[\\w-]+(?:\\.[\\w-]+){0,2})\\)");
			var matcher = EclipseProjectIdPattern.matcher(issue.getDescription());
			if (matcher.find()) {
				var id = matcher.group("id");
				logger.info("Found a project ID: {}", id);
				return id;
			}
			
			return null;
		}
		
		private String getProjectIdFromIssueTitle() {
			return null;
		}
	}
}
