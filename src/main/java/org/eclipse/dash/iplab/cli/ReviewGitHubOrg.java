/*************************************************************************
 * Copyright (c) 2024 The Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made available under
 * the terms of the Eclipse Public License 2.0 which accompanies this
 * distribution, and is available at https://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 *************************************************************************/
package org.eclipse.dash.iplab.cli;

import java.io.IOException;
import java.io.InputStream;
import java.util.function.Consumer;

import org.eclipse.dash.api.EclipseApi;
import org.eclipse.dash.api.EclipseApi.HttpService;
import org.eclipse.dash.git.GitHubService;
import org.eclipse.dash.git.IRepository;
import org.eclipse.dash.git.RepositoryService;
import org.eclipse.dash.api.Project;
import org.eclipse.dash.iplab.service.ReviewCreationService;
import org.eclipse.dash.licenses.ContentId;
import org.eclipse.dash.licenses.IProxySettings;
import org.eclipse.dash.licenses.ISettings;
import org.eclipse.dash.licenses.http.HttpClientService;
import org.eclipse.dash.licenses.http.IHttpClientService;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Issue;
import org.kohsuke.github.GitHub;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.util.Providers;

public class ReviewGitHubOrg {
	final Logger logger = LoggerFactory.getLogger(ReviewGitHubOrg.class);

	public static void main(String[] args) {
		new ReviewGitHubOrg().run();
	}
	
	public void run() {
		logger.info("Starting the processor.");
		
		Injector injector = Guice.createInjector(new ReviewModule());
		var reviewService = injector.getInstance(ReviewCreationService.class);
		var gitHub = injector.getInstance(GitHubService.class);
		var eclipse = injector.getInstance(EclipseApi.class);
		var project = eclipse.getProject("oniro.oniro4openharmony");
		
		gitHub.getRepositories("eclipse-oniro-mirrors")
			.filter(each -> !each.isEmpty())
			.filter(each -> !each.getName().startsWith("."))
			.filter(each -> findExistingReview(reviewService, each) == null)
			.limit(100)
			.forEach(each -> createReview(reviewService, project, each));
	}

	private Issue findExistingReview(ReviewCreationService reviewService, IRepository repository) {
		try {
			return reviewService.findExistingReview(new String[] {}, true, each -> {
				String name = repository.getName();
				var current = ContentId.getContentId(each.getTitle()).getName();
				return current.equals(name);
			});
		} catch (GitLabApiException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	void createReview(ReviewCreationService reviewService, Project project, IRepository repository) {
		try {
			reviewService.createReview(project, repository, new String[] {ReviewCreationService.PROJECT_CONTENT,ReviewCreationService.REVIEW_NEEDED});
		} catch (GitLabApiException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	String getIpLabUrl() {
		return System.getProperty("org.eclipse.dash.iplab.url", "https://gitlab.eclipse.org");
	}
	
	String getIpLabPath() {
		return System.getProperty("org.eclipse.dash.iplab.path");
	}
	
	String getIpLabToken() {
		return System.getProperty("org.eclipse.dash.token", "");
	}

	public class ReviewModule extends AbstractModule {

		@Override
		protected void configure() {
			bind(ISettings.class).toInstance(new ISettings() {
				@Override
				public int getTimeout() {
					// TODO Auto-generated method stub
					return 5 * 60;
				}
			});
			bind(GitLabApi.class).toInstance(new GitLabApi(getIpLabUrl(), getIpLabToken().trim()));
			
			HttpClientService httpClient = new HttpClientService();
			
			bind(EclipseApi.class).toInstance(new EclipseApi(new HttpService() {
				@Override
				public int get(String url, String contentType, Consumer<InputStream> handler) {
					return httpClient.get(url, contentType, handler);
				}
			}));
			bind(IHttpClientService.class).toInstance(httpClient);		
			bind(IProxySettings.class).toProvider(Providers.of(null));
			
			try {
				var github = GitHub.connect();
				github.checkApiUrlValidity();
				bind(GitHub.class).toInstance(github);
			} catch (IOException e) {
				throw new RuntimeException("GitHub connection not valid (check GITHUB_LOGIN and GITHUB_OAUTH).", e);
			}
			
			bind(RepositoryService.class).toInstance(new RepositoryService());
			
			bind(ReviewCreationService.class).toInstance(new ReviewCreationService(getIpLabPath()));
		}
	}

}
