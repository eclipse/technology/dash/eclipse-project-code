package org.eclipse.dash.ip.project.code;

import org.eclipse.dash.api.EclipseApi;
import org.eclipse.dash.git.GitLabService;
import org.eclipse.dash.git.RepositoryService;
import org.eclipse.dash.ip.project.code.context.ProjectCodeReviewModule;
import org.junit.jupiter.api.Test;

import com.google.inject.Guice;
import com.google.inject.Injector;

class TaskFinderTests {

	@Test
	void test() {
		
		Injector injector = Guice.createInjector(new ProjectCodeReviewModule());
		var eclipse = injector.getInstance(EclipseApi.class);
		var project = eclipse.getProject("adoptium.temurin");
		
		var service = injector.getInstance(RepositoryService.class);
		
		service.repositories(project).forEach(each -> System.out.println(each.getWebUrl()));
		
	}


	@Test
	void test2() {
		
		Injector injector = Guice.createInjector(new ProjectCodeReviewModule());
		var eclipse = injector.getInstance(GitLabService.class);
		eclipse.getGroups("eclipse/xfsc").forEach(each -> System.out.println(each));
		
	}
	

	@Test
	void test3() {
		
		Injector injector = Guice.createInjector(new ProjectCodeReviewModule());
		var gitlab = injector.getInstance(GitLabService.class);
		gitlab.getProjects("eclipse/xfsc").forEach(each -> System.out.println(each.getNameWithNamespace()));
		
	}
	

	@Test
	void test4() {
		
		Injector injector = Guice.createInjector(new ProjectCodeReviewModule());
		var gitlab = injector.getInstance(GitLabService.class);
		gitlab.getRepositories("eclipse/xfsc").forEach(each -> System.out.println(each.getBrowseUrl() + " => " + each.hasPotentiallyInterestingContent()));
		
	}
}
